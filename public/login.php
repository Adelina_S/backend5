<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  // при нажатии на кнопку Выход).
  // Делаем перенаправление на форму.
  header('Location: index.php');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if(!empty($_COOKIE['login_error'])){
    setcookie('login_error', '', 100000);
    print('Такого логина не существует.');
  }
?>

<form action="login.php" method="post">
  <input name="login" placeholder="Логин" />
  <input name="pass" placeholder="Пароль" />
  <input type="submit" value="Войти" />
</form>

<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.

  // Если все ок, то авторизуем пользователя.
  $user = 'u16306';
  $pass = '2435324';
  $db = new PDO('mysql:host=localhost;dbname=u16306', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  try{
    $stmt = $db->prepare("SELECT `login`,`pass` FROM `user` WHERE login = ?");
    $stmt->execute(array($_POST['login']));
    $result = $stmt->fetchAll();
  }
  catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
  }
  if(password_verify($_POST['pass'], $result[0][1])){
  $stmt = $db->prepare("SELECT `id` FROM `user` WHERE login = ? AND pass = ?");
  $stmt->execute(array($_POST['login'], $result[0][1]));
  $result = $stmt->fetchAll();
  $_SESSION['login'] = $_POST['login'];
  // Записываем ID пользователя.
  $_SESSION['uid'] = $result[0]["id"];

  // Делаем перенаправление.
  header('Location: index.php');
  }
  else{
    setcookie('login_error', '1', time() + 24*60*60);
    header('Location: login.php');
  }
}
